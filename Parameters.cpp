#include "Parameters.h"

Parameters::Parameters(const int temperature, const double alpha, const int steps, const int numberOfEvaluations) {
	this->temperature = (double) temperature;
	this->alpha = alpha;
	this->steps = steps;
	this->numberOfEvaluations = numberOfEvaluations;
}

Parameters::~Parameters(void) {
}

void Parameters::setupRecuit(TRecuit &recuit) const
{
	cout << "	[SETUP] Init Temperature = " << this->temperature << ", Alpha = " << this->alpha << ", Steps = " << this->steps << endl << endl;
	recuit.TempInit = this->temperature;
	recuit.Temperature = this->temperature;
	recuit.Alpha = this->alpha;
	recuit.NbPalier = this->steps;
	recuit.NB_EVAL_MAX = this->numberOfEvaluations;
	recuit.CptEval = 0;
}

string Parameters::toString() const
{
	std::ostringstream stream;
	stream << "	[BEST] Init Temperature = " << this->temperature << ", Alpha = " << this->alpha << ", Steps = " << this->steps;
	return stream.str();
}